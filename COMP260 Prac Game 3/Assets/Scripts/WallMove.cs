﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WallMove : MonoBehaviour {

	private Animator animator;
	public float startTime = 0.0f;

	void Start () {
		animator = GetComponent<Animator>();
	}
	void Update () {
		animator.SetBool("Start", Time.time >= startTime);
	}
}
