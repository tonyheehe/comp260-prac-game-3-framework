﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletMove : MonoBehaviour {

	public float speed = 10.0f;
	public Vector3 direction;
	private Rigidbody rigidbody;
	public float tooFar = 10.0f; 

	void Start () {
		rigidbody = GetComponent<Rigidbody>();    
	}

	void FixedUpdate () {
		rigidbody.velocity = speed * direction;
		if (tooFar < 0) {
			Destroy(gameObject);
		}
		tooFar -= Time.deltaTime;
	}
	void OnCollisionEnter(Collision collision) {
		Destroy(gameObject);
	}
}
