﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShellMenu : MonoBehaviour {

	private bool paused = true;
	public GameObject shellPanel;
	bool a =false;
	public GameObject optionsPanel;
	public Dropdown qualityDropdown;
	public Dropdown resolutionDropdown;
	public Toggle fullscreenToggle;
	public Slider volumeSlider;
	void Start () {
		SetPaused(paused);
		optionsPanel.SetActive (false);

		qualityDropdown.ClearOptions ();
		List<string> names = new List<string> ();
		for (int i = 0; i < QualitySettings.names.Length; i++) {
			names.Add (QualitySettings.names [i]);
		}
		resolutionDropdown.ClearOptions ();
		List<string> resolutions = new List<string> ();

		for (int i = 0; i < Screen.resolutions.Length; i++) {
			resolutions.Add (Screen.resolutions [i].ToString ());
		}
		resolutionDropdown.AddOptions (resolutions);
		if (PlayerPrefs.HasKey ("AudioVolume")) {
			AudioListener.volume = PlayerPrefs.GetFloat ("AudioVolume");
		} else {
			AudioListener.volume = 1;
		}
	}

	void Update () {
		
		if (!paused && Input.GetKeyDown (KeyCode.Escape)) {
			Debug.Log ("p");
			SetPaused (true);
		}
		
	}
	private void SetPaused(bool p) {
		// make the shell panel (in)active when (un)paused
		paused = p;
		shellPanel.SetActive(paused);
		Time.timeScale = paused ? 0 : 1;
	}
	public void OnPressedPlay() {
		// resume the game
		SetPaused(false);

	}

	public void OnPressedQuit() {
		// quit the game
		Application.Quit();
	}

	public void OnPressedOption(){
		shellPanel.SetActive (false);
		optionsPanel.SetActive (true);

		qualityDropdown.value = QualitySettings.GetQualityLevel ();

		int currentResolution = 0;
		for (int i = 0; i < Screen.resolutions.Length; i++) {
			if (Screen.resolutions [i].width == Screen.width &&
			   Screen.resolutions [i].height == Screen.height) {
				currentResolution = 1;
				break;
			}
		}
		fullscreenToggle.isOn = Screen.fullScreen;
		volumeSlider.value = AudioListener.volume;
	}
	public void OnPressCancel(){
		shellPanel.SetActive (true);
		optionsPanel.SetActive (false);
	}
	public void OnPressApply(){

		QualitySettings.SetQualityLevel (qualityDropdown.value);
		shellPanel.SetActive (true);
		optionsPanel.SetActive (false);
		Resolution res = 
			Screen.resolutions [resolutionDropdown.value];
		Screen.SetResolution (res.width, res.height, fullscreenToggle.isOn);
		AudioListener.volume = volumeSlider.value;
		PlayerPrefs.SetFloat ("AudioVolume", AudioListener.volume);
	}


}
