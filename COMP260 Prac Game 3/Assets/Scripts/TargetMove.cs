﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TargetMove : MonoBehaviour {
	private Animator animator;
	public float startTime = 0.0f;

	void Start () {
		animator = GetComponent<Animator>();
	}
	void Update () {
		animator.SetBool("Start", Time.time >= startTime);
	}
	void Destroy() {
		Destroy(gameObject);
	}
	void OnCollisionEnter(Collision collision){
		animator.SetBool ("Hit", true);
		Debug.Log ("hit");
	}
}
