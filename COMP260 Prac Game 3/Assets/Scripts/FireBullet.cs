﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireBullet : MonoBehaviour {

	// Use this for initialization
	public BulletMove bulletPrefab;
	public float Reload = 2.0f;
	private float downtime;
	private bool ready;
	void start(){
		downtime = Reload;
		ready = true;
	}
	void Update () {
		if (Time.timeScale == 0) {
			return;
		}

		if (downtime < Reload) {
			downtime += Time.deltaTime;
		} else {
			ready = true;
		}
		if(Input.GetButtonDown ("Fire1") && ready){

				BulletMove bullet = Instantiate (bulletPrefab);

				bullet.transform.position = transform.position;


				Ray ray = 
					Camera.main.ScreenPointToRay (Input.mousePosition);
				bullet.direction = ray.direction;
				downtime = 0;
				ready = false;
		}
	}
}
